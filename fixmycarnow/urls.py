from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from django.contrib.staticfiles.storage import staticfiles_storage
from django.views.generic.base import RedirectView


from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns(
    '',
    url(r'^favicon.ico$',
        RedirectView.as_view(
            url=staticfiles_storage.url('favicon.ico'),
            permanent=False),
        name="favicon"),
    url(r'^$', 'car.views.index'),
    url(r'^settings/$', 'car.views.settings'),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
    url(r'^news/', include('car.urls')),
    url(r'^friends/', 'car.views.friends'),
    url(r'^add_post/$', 'car.views_add.add_post'),
    url(r'^add_user/$', 'car.views_add.add_user'),
    url(r'^add_vehicle/$', 'car.views_add.add_vehicle'),
    url(r'^edit_vehicle/$', 'car.views_edit.edit_vehicle'),
    url(r'^edit_user/$', 'car.views_edit.edit_user'),
    url(r'^edit_vehicle_details/$', 'car.views_edit.edit_vehicle_details'),
    url(r'^add_carcompany/$', 'car.views_add.add_carcompany'),
    url(r'^save_job/$', 'car.views_add.add_job'),
    url(r'^add_job_detail_item/$', 'car.views_add.add_job_detail_item'),
    url(r'^initialize/$', 'car.views_add.initialize'),
    url(r'^vehicles/$', 'car.views.vehicles'),
    url(r'^vehicle/$', 'car.views.vehicle'),
    url(r'^add_vehicle/get_model/$', 'car.views.get_model'),
    url(r'^add_vehicle_extra_detail/$', 'car.views_add.add_vehicle_extra_detail'),
    url(r'^get_model/$', 'car.views.get_model'),
    url(r'^get_friends/$', 'car.views.get_friends'),
    url(r'^get_vehicle_select/$', 'car.views.get_vehicle_select'),
    url(r'^get_vehicle_parts_table/$', 'car.views.get_vehicle_parts_table'),
    url(r'^get_user_edit_modal_div/$', 'car.views.get_user_edit_modal_div'),
    url(r'^get_user_info_table_div/$', 'car.views.get_user_info_table_div'),
    url(r'^del_vehicle_detail/$', 'car.views_del.del_vehicle_detail'),
    url(r'^job/$', 'car.views.job'),
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()
