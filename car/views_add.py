import datetime
import json
import os

from urlparse import parse_qs

import views

from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.db import IntegrityError
from django.conf import settings

from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from car.models import Vehicle
from car.models import VehicleExtraDetails
from car.models import CarCompany
from car.models import CarModel
from car.models import Job
from car.models import JobDetails
from car.models import Post

from django.shortcuts import redirect


@csrf_protect
@login_required()
def add_post(request):
    try:
        post_message = request.POST['post_message']
    except KeyError:
        return HttpResponseBadRequest()
    try:
        vehicle_message = request.POST['vehicle_message']
    except KeyError:
        return HttpResponseBadRequest()

    vehicle_object = Vehicle.objects.get(id=vehicle_message)

    print post_message
    print request.user.id

    new_post = Post(user=request.user,
                    vehicle=vehicle_object,
                    message=post_message)

    new_post.save()

    return HttpResponse('done')


@csrf_protect
@login_required()
def add_job_detail_item(request):
    added = None
    error = None

    if request.method == "POST":
        print 'It is a post request'
        name = request.POST['name']
        category = request.POST['category']
        saving_job_details(name=name, category=category)

    detail_list = []

    categories = JobDetails.objects.order_by('category').values('category').distinct()
    for item in categories:
        detail_list.append({'text': item['category'], 'value': item['category']})

    items = [
        {'type': 'text', 'id': 'name', 'label': 'Item Name', 'placeholder': 'Name'},
        {'type': 'dropdown_no_search', 'id': 'category', 'label': 'Category',
         'options': detail_list},
    ]
    context = RequestContext(request, {
        'submit_to': '/add_job_detail_item/',
        'back_to': '/friends/',
        'items': items,
        'added': added,
        'error': error,
    })

    template = loader.get_template('add.html')

    return HttpResponse(template.render(context))


def init_job_details():
    if JobDetails.objects.count() > 0:
        return 'Database not empty'

    print 'init job details'
    cwd = os.path.abspath(__file__)
    path = os.path.join('car', 'job_details_init.csv')
    with open(path, 'r') as jdi:
        for line in jdi.readlines():
            item = line.split(',')
            detail_id = item[0]
            name = item[1].replace('\"', '').title()
            category = item[2].replace('\"', '').title().strip()
            saving_job_details(detail_id, name, category)

    return 'Initialization complete'


def saving_job_details(detail_id, name, category):
    job_detail = JobDetails(id=detail_id, name=name, category=category)
    job_detail.save()


@csrf_protect
@login_required()
def add_job(request):
    if request.method == 'POST':
        print request.POST
        data = parse_qs(request.POST['job_details'].encode('ASCII'))
        print data
        v_id = request.POST['vehicle_id']
        print('got vehcile id: %s' % v_id)
        vehicle = Vehicle.objects.get(id=int(request.POST['vehicle_id']))
        print('got vehicle from db')
        print('got user id: %s' % vehicle.user.id)
        user = User.objects.get(id=vehicle.user.id)
        print('got user from db')
        all_job_details = JobDetails.objects.all()
        print('got all job details')

        try:
            date_created = datetime.datetime.strptime(data['date_created'][0], '%B %d, %Y')
        except ValueError:
            print('ValueError')
            date_created = datetime.datetime.now()

        print('got date created: %s' % date_created)

        mileage = int(data['mileage'][0]) if 'mileage' in data else vehicle.mileage
        print('mileage is set to %s' % mileage)

        note = data['note'][0] if 'note' in data else ''
        if 'job_id' not in data:
            job = Job(mileage=int(mileage), note=note, vehicle=vehicle, user=user,
                      last_updated=datetime.datetime.now())
            response_message = 'Job added successfully'
        else:
            job = Job.objects.get(id=data['job_id'][0])
            response_message = 'Job updated successfully'

        job.save()
        job.date_created = date_created
        job.save()
        job.details.clear()
        for job_detail in all_job_details:
            if 'jb_%s' % job_detail.id in data:
                print '%s: %s' % (job_detail.name, data['jb_%s' % job_detail.id][0])
                jd = JobDetails.objects.get(id=job_detail.id)
                job.details.add(jd)
        job.save()

        if int(mileage) > vehicle.mileage:
            vehicle.mileage = int(mileage)
        vehicle.last_updated = datetime.datetime.now()
        vehicle.save()

    return HttpResponse(response_message)


def create_user(username, fn, ln, email='', password=None):
    user = User(username=username,
                first_name=fn,
                last_name=ln,
                email=email,
                )
    if not password:
        if settings.DEBUG:
            password = '123'
        else:
            password = 'fixmycarplease'

    user.set_password(password)
    try:
        user.save()
        print 'User %s created' % username
        return user
    except IntegrityError:
        print 'Username "%s" already exists' % username
        return None


@csrf_protect
@login_required()
def add_user(request):
    added = None
    error = None
    username = ''
    first_name = ''
    last_name = ''
    email = ''
    if request.method == "POST":
        print 'It is a post request'
        username = request.POST['username']
        password = request.POST['pass']
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        email = request.POST['email']

        new_user = create_user(username, first_name, last_name, email, password)

        if not new_user:
            error = 'Unable to create user: %s' % username
        else:
            return redirect('/vehicles?client_id=%s' % new_user.id)
            error = 'User %s was successfully created' % username

    items = [
        {'type': 'text', 'id': 'username', 'label': 'Username', 'placeholder': 'Username', 'value': username},
        {'type': 'password', 'id': 'pass', 'label': 'Password', 'placeholder': 'Password'},
        {'type': 'text', 'id': 'first_name', 'label': 'First Name', 'placeholder': 'First Name', 'value': first_name},
        {'type': 'text', 'id': 'last_name', 'label': 'Last Name', 'placeholder': 'Last Name', 'value': last_name},
        {'type': 'email', 'id': 'email', 'label': 'Email address', 'placeholder': 'Email address', 'value': email},
    ]
    context = RequestContext(request, {
        'submit_to': '/add_user/',
        'back_to': '/friends/',
        'user': request.user,
        'items': items,
        'added': added,
        'error': error,
    })

    template = loader.get_template('add.html')
    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def add_carcompany(request):
    """

    :param request:
    :return:
    """
    error = None
    added = None
    if not request.user.is_superuser or not request.user.is_staff:
        error = 'Only administrator can add car maker company'
    if request.method == 'POST' and (request.user.is_superuser or request.user.is_staff):
        company = request.POST['company']
        company = company.upper()
        carcompany = CarCompany(name=company)
        carcompany.save()

        model = request.POST['model']
        carmodel = CarModel(name=model, car_company_id=carcompany.id)
        carmodel.save()
        added = '%s %s' % (carcompany.name, carmodel.name)

    all_companies = CarCompany.objects.all()

    items = [
        {'type': 'text', 'id': 'company', 'label': 'Car Company', 'placeholder': 'Car Company'},
        {'type': 'text', 'id': 'model', 'label': 'Car Model', 'placeholder': 'Car Model'},
    ]
    context = RequestContext(request, {
        'submit_to': '/add_carcompany/',
        'back_to': '/add_carcompany/',
        'user': request.user,
        'items': items,
        'added': added,
        'error': error,
        'companies': all_companies,
    })

    template = loader.get_template('add_company_model.html')
    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def initialize(request):
    init_type = request.GET.get('type', None)
    if init_type == 'job_details':
        message = init_job_details()
    elif init_type == 'make_model':
        message = init_make_model()
    elif init_type == 'users':
        message = init_users()
    elif init_type == 'all':
        init_job_details()
        init_make_model()
        init_users()
        message = 'Full initialization complete'
    else:
        message = 'Unknown choice'

    context = RequestContext(request, {
        'result': message
    })
    template = loader.get_template('settings.html')

    return HttpResponse(template.render(context))


def init_users():
    """
    :return:
    """
    print 'init users'
    c_path = os.path.join('car', 'old_clients.json')
    v_path = os.path.join('car', 'old_vehicles.json')
    j_path = os.path.join('car', 'jobs_init.json')

    with open(v_path, 'r') as v_file:
        all_vehicles = json.load(v_file)
    with open(j_path, 'r') as j_file:
        all_jobs = json.load(j_file)

    with open(c_path, 'r') as old_users:
        all_users = json.load(old_users)
        for old_user in all_users:
            print old_user['Name']
            split_name(old_user['Name'])
            username, first_name, last_name = split_name(old_user['Name'])
            old_email = old_user['Email']
            user = create_user(username, first_name, last_name, old_email)
            if not user:
                user = User.objects.get(username=username)
            for vehicle in all_vehicles:
                if vehicle['ClientID'] == old_user['ID']:
                    new_vehicle = create_vehicle(vehicle, user)
                    for job in all_jobs:
                        if job['VehicleID'] == vehicle['ID']:
                            create_job(new_vehicle, job)

    return 'User initialization Done'


def create_job(vehicle, job):
    jdl = job['job_details'].split(',')
    from datetime import datetime

    job_date = datetime.strptime(job['JobDate'], '%m/%d/%Y')
    print job_date

    job_object = Job(
        user=vehicle.user,
        vehicle=vehicle,
        mileage=job['CurrentMileage'],
        date_created=job_date,
        last_updated=job_date,
        note=job['History']
    )
    job_object.save()

    for j in jdl:
        jd_object = JobDetails.objects.get(id=j)
        job_object.details.add(jd_object)
    job_object.save()

    job_object.date_created = job_date
    job_object.last_updated = job_date

    job_object.save()


def create_vehicle(vehicle, user):
    try:
        make_object = CarCompany.objects.get(name=vehicle['Make'].upper())
    except:
        make_object = CarCompany(name=vehicle['Make'].upper())
        make_object.save()

    try:
        model_object = CarModel.objects.get(name=vehicle['Model'].upper())
    except:
        model_object = CarModel(car_company=make_object, name=vehicle['Model'].upper())
        model_object.save()

    user_object = User.objects.get(id=user.id)
    print 'user: %s' % user_object
    print 'make: %s' % make_object.name
    print 'model: %s' % model_object.name
    print 'model id: %s' % model_object.id

    if vehicle['Synthetic'] == '1':
        oil_type = 'S'
    else:
        oil_type = 'R'

    new_vehicle = Vehicle(
        user=user_object,
        license_plate=vehicle['LPlate'],
        make=make_object,
        model=model_object,
        color=vehicle['Color'],
        year=vehicle['Yr'],
        mileage=vehicle['Mileage'],
        engine_oil_type=oil_type,
        note=vehicle['History'],
    )

    new_vehicle.save()

    return new_vehicle


def split_name(name):
    name = name.replace('-', ' ')
    name_split = name.split(' ')
    if len(name_split) > 1:
        last_name = name_split.pop(len(name_split) - 1)
    else:
        last_name = ''
    first_name = ' '.join(name_split)
    username = ''.join(name_split).lower() + last_name.lower()
    print 'username: %s' % username
    print 'first name: %s' % first_name
    print 'last name: %s' % last_name

    return username, first_name, last_name


def init_make_model():
    print 'init car make and model'
    path = os.path.join('car', 'json_data.json')
    with open(path, 'r') as jdi:
        make_model_json = json.load(jdi)
        for make_json in make_model_json:
            print make_json['make'] + ' ' + make_json['model']
            make = make_json['make']
            try:
                make_db = CarCompany.objects.get(name=make)
            except:
                make_db = CarCompany(name=make)
                make_db.save()

            model_name = make_json['model']
            try:
                model_db = CarModel.objects.get(name=model_name)
            except:
                model_db = CarModel(car_company=make_db, name=model_name)
                try:
                    model_db.save()
                except:
                    print 'name too long: %s' % model_name

    return 'Make Model Initialization complete'


@csrf_protect
@login_required()
def add_vehicle(request):
    """
    :param request:
    :return:
    """
    added = None

    if request.method == 'GET':
        if request.user.is_staff:
            client_id = int(request.GET['client_id'])
            client = User.objects.get(id=client_id)
        else:
            client_id = request.user.id
            client = request.user

    if request.method == 'POST' and (request.user.is_staff or int(request.POST['client_id']) == request.user.id):
        client_id = int(request.POST['client_id'])
        user_id = int(request.POST['user'])
        client = User.objects.get(id=client_id)
        added_for = User.objects.get(id=user_id)
        client_id = int(request.POST['client_id'])
        engine_oil_type = request.POST['engine_oil_type'][0]
        print request.POST['make']
        print request.POST['model']
        make = CarCompany.objects.get(id=request.POST['make'])
        model = CarModel.objects.get(id=request.POST['model'])
        mileage = 0 if request.POST['mileage'] == '' else request.POST['mileage']

        vehicle = Vehicle(
            user=added_for,
            license_plate=request.POST['license_plate'].upper(),
            make=make,
            model=model,
            year=request.POST['year'],
            mileage=mileage,
            color=request.POST['color'],
            note=request.POST['note'],
            engine_oil_type=engine_oil_type,
        )
        vehicle.save()
        return redirect('/vehicles?client_id=%s' % client_id)

    company_list = CarCompany.objects.all().order_by('name')
    company_list_options = []
    for company in company_list:
        company_list_options.append({'value': company.id,
                                     'text': company.name})
    model_list = CarModel.objects.filter(car_company__id=1)
    models = []
    for model in model_list:
        print model.name
        models.append({'value': model.id,
                       'text': model.name})
    year_list = []
    from datetime import date
    for year in range(1950, date.today().year + 1):
        year_list.append({'value': year,
                          'text': year})

    user_list = []
    all_users = User.objects.filter(id__gt=1).order_by('last_name')
    for user in all_users:
        selected = 'selected' if user.id == client_id else ''
        if request.user.is_staff or user.id == request.user.id:
            user_list.append({'value': user.id,
                              'text': '%s %s' % (user.first_name, user.last_name),
                              'selected': selected})
    items = [
        {'type': 'dropdown', 'id': 'user', 'label': 'Friend',
         'options': user_list},
        {'type': 'text', 'id': 'license_plate', 'label': 'License Plate Number', 'type': 'text',
         'placeholder': 'License Plate Number'},
        {'type': 'text', 'id': 'make', 'label': 'Make',
         'options': company_list_options, 'placeholder': 'Make'},
        {'type': 'model', 'id': 'model', 'label': 'Model', 'placeholder': 'Model', 'options': models},
        {'type': 'dropdown', 'id': 'year', 'label': 'Year',
         'options': reversed(year_list)},
        {'type': 'text', 'id': 'color', 'label': 'Color', 'placeholder': 'Body color'},
        {'type': 'number', 'id': 'mileage', 'label': 'Mileage', 'placeholder': 'Current Mileage'},
        {'type': 'dropdown', 'id': 'engine_oil_type', 'label': 'Engine Oil Type',
         'options': [{'value': 'Regular', 'text': 'Regular'},
                     {'value': 'Synthetic', 'text': 'Synthetic'}]},
        {'type': 'textarea', 'id': 'note', 'label': 'Note', 'placeholder': 'Note'},
    ]
    hidden_items = [
        {'id': 'client_id', 'value': client_id}
    ]

    context = RequestContext(request, {
        'submit_to': '/add_vehicle/',
        'back_to': '/vehicles?client_id=%s' % client_id,
        'user': request.user,
        'items': items,
        'hidden_items': hidden_items,
        'client': client,
        'added': added,
    })

    template = loader.get_template('add_vehicle.html')
    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def add_vehicle_extra_detail(request):
    print request.POST
    try:
        key = request.POST['key']
    except KeyError:
        return HttpResponseBadRequest()
    print key
    try:
        value = request.POST['value']
    except KeyError:
        return HttpResponseBadRequest()
    print value
    try:
        vehicle_id = int(request.POST['vehicle_id'])
    except KeyError:
        return HttpResponseBadRequest()
    print vehicle_id

    new_detail = VehicleExtraDetails(key=key, value=value)
    new_detail.save()
    print 'new detail saved'

    vehicle_object = Vehicle.objects.get(id=vehicle_id)
    vehicle_object.extra_details.add(new_detail)
    vehicle_object.save()
    print 'vehicle saved'

    # context = RequestContext(request, {
    #     'vehicle': vehicle_object
    # })
    #
    # template = loader.get_template('edit_vehicle_details.html')
    #
    # return HttpResponse(template.render(context))

    html = views.vehicle_parts_table(vehicle_id)
    print html

    return HttpResponse(html)

