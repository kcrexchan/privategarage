from django.db import models
from django.contrib.auth.models import User


class CarCompany(models.Model):
    name = models.CharField(max_length=30)


class CarModel(models.Model):
    car_company = models.ForeignKey(CarCompany)
    name = models.CharField(max_length=255)


class VehicleExtraDetails(models.Model):
    key = models.CharField(max_length=255)
    value = models.CharField(max_length=1024)
    deleted = models.BooleanField(default=False)


class Vehicle(models.Model):
    priority = models.IntegerField(default=0)
    active = models.BooleanField(default=True)
    date_added = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User)
    license_plate = models.CharField(max_length=50)
    make = models.ForeignKey(CarCompany)
    model = models.ForeignKey(CarModel)
    color = models.CharField(max_length=30)
    year = models.IntegerField()
    mileage = models.BigIntegerField()
    extra_details = models.ManyToManyField(VehicleExtraDetails)
    REGULAR = 'R'
    SYNTHETIC = 'S'
    ENGINE_OIL_TYPE_CHOICE = (
        (REGULAR, 'Regular'),
        (SYNTHETIC, 'Synthetic'),
    )
    engine_oil_type = models.CharField(max_length=2, choices=ENGINE_OIL_TYPE_CHOICE, default=REGULAR)
    note = models.TextField()

    def get_oil_type(self):
        return 'Regular' if self.engine_oil_type == 'R' else 'Synthetic'


class Post(models.Model):
    user = models.ForeignKey(User)
    vehicle = models.ForeignKey(Vehicle)
    date_created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255)
    message = models.CharField(max_length=3000)
    mark_read = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)


class JobDetails(models.Model):
    name = models.CharField(max_length=255)
    category = models.CharField(max_length=255)


class Job(models.Model):
    deleted = models.BooleanField(default=False)
    user = models.ForeignKey(User)
    vehicle = models.ForeignKey(Vehicle)
    mileage = models.BigIntegerField()
    date_created = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField()
    details = models.ManyToManyField(JobDetails)
    note = models.TextField()

