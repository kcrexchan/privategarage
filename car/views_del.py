import views

from django.http import HttpResponse
from django.http import HttpResponseBadRequest

from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from car.models import VehicleExtraDetails

from django.core.exceptions import ObjectDoesNotExist
from car.models import Job


def del_job(job_id):
    """ Delete a job of a vehicle

    :param job_id:
    :return: True if job is deleted; False if job id does not exist
    """

    try:
        job = Job.objects.get(id=job_id)
    except ObjectDoesNotExist:
        print('Unable to find job with job_id: %s' % job_id)
        return -1
    else:
        job.deleted = True
        job.save()
        return job_id

    return -1


@csrf_protect
@login_required()
def del_vehicle_detail(request):
    """

    :param request:
    :return:
    """
    try:
        vehicle_id = int(request.POST['vehicle_id'])
    except KeyError:
        return HttpResponseBadRequest()

    try:
        detail_id = int(request.POST['detail_id'])
    except KeyError:
        return HttpResponseBadRequest()

    detail_object = VehicleExtraDetails.objects.get(id=detail_id)
    detail_object.deleted = True
    detail_object.save()

    html = views.vehicle_parts_table(vehicle_id)

    return HttpResponse(html)
