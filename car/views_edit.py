from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseNotAllowed

from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from car.models import Vehicle
from car.models import CarCompany


@csrf_protect
@login_required()
def edit_user(request):
    """

    :param request:
    :return:
    """
    if request.method == 'POST':
        user_object = User.objects.get(id=request.user.id)
        post = request.POST
        print user_object.username

        user_object.first_name = post['first_name']
        user_object.last_name = post['last_name']
        user_object.email = post['email']
        if post['password']:
            user_object.set_password(post['password'])
        user_object.save()

        context = RequestContext(request, {
            'client': user_object
        })
        template = loader.get_template('user_info_table_div.html')

        return HttpResponse(template.render(context))
    else:
        return HttpResponseNotAllowed()


@csrf_protect
@login_required()
def edit_vehicle(request):
    """
    :param request:
    :return:
    """
    edited = None

    if request.method == 'GET':
        vehicle_id = int(request.GET['vehicle_id'])
        vehicle = Vehicle.objects.get(id=vehicle_id)
    else:
        vehicle_id = int(request.POST['vehicle_id'])
        license_plate = request.POST['license_plate'],
        make = CarCompany.objects.get(id=request.POST['make'])
        model = request.POST['model'],
        year = request.POST['year'],
        color = request.POST['color'],
        mileage = request.POST['mileage'],
        note = request.POST['note'],
        engine_oil_type = request.POST['engine_oil_type'][0]

        vehicle = Vehicle.objects.get(id=vehicle_id)
        vehicle.license_plate = license_plate[0].upper()
        vehicle.make = make
        vehicle.model = model[0].upper()
        vehicle.year = year[0]
        vehicle.color = color[0]
        vehicle.mileage = mileage[0]
        print note[0]
        vehicle.note = note[0]
        vehicle.engine_oil_type = engine_oil_type

        vehicle.save()
        edited = '%s %s' % (vehicle.make.name, vehicle.model)

    company_list = CarCompany.objects.all().order_by('name')
    company_list_options = []
    for company in company_list:
        selected = 'selected' if vehicle.make == company else ''
        company_list_options.append({'value': company.id,
                                     'text': company.name,
                                     'selected': selected})
    year_list = []
    from datetime import date
    for year in range(1950, date.today().year + 1):
        selected = 'selected' if int(vehicle.year) == year else ''
        year_list.append({'value': year,
                          'text': year,
                          'selected': selected})

    user = User.objects.get(id=vehicle.user.id)

    items = [
        {'type': 'text', 'id': 'user', 'label': 'Friend', 'value': '%s %s' % (user.first_name, user.last_name),
         'disabled': 'disabled'},
        {'type': 'text', 'id': 'license_plate', 'label': 'License Plate Number', 'type': 'text',
         'placeholder': 'License Plate Number', 'value': vehicle.license_plate},
        {'type': 'dropdown', 'id': 'make', 'label': 'Make',
         'options': company_list_options},
        {'type': 'text', 'id': 'model', 'label': 'Model', 'placeholder': 'Model', 'value': vehicle.model},
        {'type': 'dropdown', 'id': 'year', 'label': 'Year',
         'options': reversed(year_list)},
        {'type': 'text', 'id': 'color', 'label': 'Color', 'placeholder': 'Body color', 'value': vehicle.color},
        {'type': 'text', 'id': 'mileage', 'label': 'Mileage', 'placeholder': 'Current Mileage',
         'value': vehicle.mileage},
        {'type': 'dropdown', 'id': 'engine_oil_type', 'label': 'Engine Oil Type',
         'options': [{'value': 'Regular', 'text': 'Regular',
                      'selected': 'selected' if vehicle.engine_oil_type == 'R' else ''},
                     {'value': 'Synthetic', 'text': 'Synthetic',
                      'selected': 'selected' if vehicle.engine_oil_type == 'S' else ''}]},
        {'type': 'textarea', 'id': 'note', 'label': 'Note', 'placeholder': 'Note', 'value': vehicle.note},
    ]
    hidden_items = [
        {'id': 'vehicle_id', 'value': vehicle_id}
    ]

    context = RequestContext(request, {
        'submit_to': '/edit_vehicle/',
        'back_to': '/vehicles?client_id=%s' % vehicle.user.id,
        'user': request.user,
        'items': items,
        'hidden_items': hidden_items,
        'vehicle': vehicle,
        'added': edited,
    })

    template = loader.get_template('edit.html')
    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def edit_vehicle_details(request):
    if request.method == 'POST':
        print request.POST
        try:
            vehicle_id = int(request.POST['vehicle_id'])
            mileage = int(request.POST['mileage'])
            year = int(request.POST['year'])
            oil_type = request.POST['oil_type']
            active = request.POST['active'] == 'true'
        except KeyError:
            return HttpResponseBadRequest('Failed to save vehicle details. Parameter(s) missing.')
        else:
            vehicle_object = Vehicle.objects.get(id=vehicle_id)
            vehicle_object.mileage = mileage
            vehicle_object.year = year
            vehicle_object.engine_oil_type = oil_type
            vehicle_object.active = active
            vehicle_object.save()
            response = 'Vehicle details saved'

        return HttpResponse(response)

    vehicle_id = int(request.GET.get('vehicle_id', None))
    if not vehicle_id:
        return HttpResponseBadRequest()

    vehicle_object = Vehicle.objects.get(id=vehicle_id)

    context = RequestContext(request, {
        'vehicle': vehicle_object
    })

    template = loader.get_template('edit_vehicle_details.html')

    return HttpResponse(template.render(context))
