import json
import datetime

import views_del

from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.core.exceptions import ObjectDoesNotExist

from django.views.decorators.csrf import csrf_protect
from django.template import Context
from django.template import RequestContext
from django.template import loader
from django.contrib.auth.decorators import login_required

from car.models import Vehicle
from car.models import Job
from car.models import JobDetails
from car.models import CarCompany
from car.models import CarModel
from car.models import Post
from django.contrib.auth.models import User


def get_vehicles(user):
    """

    :param user:
    :return:
    """
    if user.is_superuser:
        vehicle_objects = Vehicle.objects.all().order_by('-last_updated')
    else:
        vehicle_objects = Vehicle.objects.filter(user=user).order_by('-last_updated')

    return vehicle_objects


@login_required()
def index(request):
    """

    :param request:
    :return:
    """
    show_recent = 12
    try:
        delete_id = request.GET['delete']
    except KeyError:
        pass
    else:
        try:
            post_object = Post.objects.get(id=delete_id)
        except Post.DoesNotExist:
            return HttpResponseBadRequest()
        else:
            post_object.deleted = True
            post_object.save()

    try:
        mark_read_id = request.GET['mark_read']
    except KeyError:
        pass
    else:
        try:
            post_object = Post.objects.get(id=mark_read_id)
        except Post.DoesNotExist:
            return HttpResponseBadRequest
        else:
            post_object.mark_read = True
            post_object.save()

    try:
        mark_unread_id = request.GET['mark_unread']
    except KeyError:
        pass
    else:
        try:
            post_object = Post.objects.get(id=mark_unread_id)
        except Post.DoesNotExist:
            return HttpResponseBadRequest
        else:
            post_object.mark_read = False
            post_object.save()

    if request.user.is_superuser:
        job_oc_list = []
        job_objects = Job.objects.filter(deleted=False).order_by('-date_created')[:show_recent]

        jd_oc = JobDetails.objects.filter(name__in=['Oil Change'])
        vehicles_qs = Vehicle.objects.filter(active=True, priority__gte=0)
        print vehicles_qs.count()
        for v in vehicles_qs:
            try:
                oc_job = Job.objects.filter(details__in=jd_oc, deleted=False, vehicle=v).latest('last_updated')
                timediff = datetime.datetime.now() - oc_job.last_updated.replace(tzinfo=None)
                if 180 < timediff.days < 365:
                    oc_job_extra = {
                        'original': oc_job,
                        'months_diff': timediff.days / 30
                    }
                    job_oc_list.append(oc_job_extra)
            except ObjectDoesNotExist:
                continue

    else:
        job_objects = Job.objects.filter(deleted=False, user=request.user).order_by('-date_created')[:show_recent]
        job_oc_list = None

    vehicle_list = get_vehicles(request.user)

    post_objects = get_posts(request.user)
    for post_object in post_objects:
        print post_object.message

    context = RequestContext(request,
                             {'jobs': job_objects,
                              'oc_jobs': job_oc_list,
                              'posts': post_objects,
                              'vehicle_list': vehicle_list})
    template = loader.get_template('news.html')

    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def settings(request):
    if not request.user.is_staff or not request.user.is_superuser:
        return HttpResponse('Please log in as Admin to view this page.')

    context = Context({})
    template = loader.get_template('settings.html')
    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def get_friends(request):
    users_queryset = User.objects.filter(id__gt=1).order_by('-date_joined')
    users = []
    for user_queryset in users_queryset:
        users.append({'id': user_queryset.id,
                      'username': user_queryset.username})

    print json.dumps(users)

    return HttpResponse(json.dumps(users))


@csrf_protect
@login_required()
def get_model(request):
    """

    :param request:
    :return:
    """
    def create_html(models):
        all_models = [
            '<datalist id="carmodels"',
        ]
        if models is not None:
            for model in models:
                option_line = '<option value="%s"></option>' % model.name
                all_models.append(option_line)
        all_models = all_models + ['</datalist>']

        return all_models

    print 'get_model'
    print request.POST['cat'].upper()
    try:
        make = CarCompany.objects.get(name=request.POST['cat'].upper())
    except:
        print('exception')

    print('after getting make')
    models = CarModel.objects.filter(car_company=make)
    print('after getting model')
    all_models = [
        '<datalist id="carmodels">',
    ]
    for model in models:
        option_line = '<option value="%s">' % model.name
        all_models.append(option_line)

    all_models = all_models + [
        '</datalist>',
    ]

    all_models = '\n'.join(all_models)

    return HttpResponse(all_models)


@login_required()
@csrf_protect
def job(request):
    """

    :param request:
    :return:
    """
    # job_obj = None
    category = []
    # is_new_job = False
    details = None
    render_data = {}

    dist_category = JobDetails.objects.values('category').distinct().order_by('category')
    for c in dist_category:
        category.append(c['category'])

    if request.method == 'POST':
        edit_job(request)

    if 'job_id' not in request.GET:
        render_data['new'] = True
        vehicle_id = request.GET['vehicle_id']
        vehicle_obj = Vehicle.objects.get(id=vehicle_id)
        user = User.objects.get(id=vehicle_obj.user.id)
    else:
        job_id = int(request.GET['job_id'])
        job_obj = Job.objects.get(id=job_id)
        vehicle_obj = Vehicle.objects.get(id=job_obj.vehicle.id)
        user = User.objects.get(id=job_obj.user.id)

        details = job_obj.details.all()
        render_data['job'] = job_obj
        render_data['job_id'] = job_id

    jd_list = {}
    for item in category:
        jd_list[item] = []
        job_details = JobDetails.objects.filter(category=item).order_by('name')
        for job_detail in job_details:
            if details:
                checked = 'checked' if job_detail in details else ''
            else:
                checked = False
            jd_list[item].append({'name': job_detail.name,
                                  'id': job_detail.id,
                                  'checked': checked,
                                  'category': item})

    render_data['vehicle'] = vehicle_obj
    render_data['job_details'] = jd_list
    render_data['user'] = user

    template = loader.get_template('job.html')
    context = RequestContext(request, render_data)

    return HttpResponse(template.render(context))


def edit_job(request):
    all_job_details = JobDetails.objects.all()

    try:
        print request.POST['date_created']
        date_created = datetime.datetime.strptime(request.POST['date_created'], '%B %d, %Y')
        print date_created
    except ValueError:
        date_created = None
    mileage = int(request.POST['mileage']) if request.POST['mileage'] else 0
    job_id = int(request.GET['job_id'])

    mod_job = Job.objects.get(id=job_id)
    if mod_job.mileage < mileage:
        mod_job.mileage = mileage
    if date_created:
        print 'Updating date created %s' % date_created
        mod_job.date_created = date_created
    else:
        print 'Date created not modified %s' % date_created
    mod_job.last_updated = datetime.datetime.now()
    mod_job.note = request.POST['note']
    mod_job.details.clear()
    mod_job.save()
    for job_detail in all_job_details:
        if 'jb_%s' % job_detail.id in request.POST:
            print '%s: %s' % (job_detail.name, request.POST['jb_%s' % job_detail.id])
            jd = JobDetails.objects.get(id=job_detail.id)
            mod_job.details.add(jd)
    mod_job.save()
    mod_job.vehicle.last_updated = datetime.datetime.now()
    mod_job.vehicle.save()


@login_required()
@csrf_protect
def vehicle(request):
    if 'vehicle_details_expanded' in request.GET:
        expanded = 'in'
    else:
        expanded = ''

    if 'vehicle_id' in request.POST:
        print request.POST['vehicle_id']
    vehicle_id = int(request.GET['vehicle_id'])

    if 'delete_job' in request.GET:
        job_id = int(request.GET['delete_job'])
        job_deleted = views_del.del_job(job_id)
    else:
        job_deleted = -1

    vehicle_object = Vehicle.objects.get(id=vehicle_id)
    vehicle_jobs = Job.objects.filter(vehicle__id=vehicle_id, deleted=False).order_by("-mileage")

    context = RequestContext(request, {
        'expanded': expanded,
        'job_deleted': job_deleted,
        'vehicle': vehicle_object,
        'jobs': vehicle_jobs,
    })

    template = loader.get_template('vehicle.html')
    return HttpResponse(template.render(context))


@login_required()
def vehicles(request):
    if request.user.is_superuser or request.user.is_staff:
        vehicles_list = Vehicle.objects.filter(
            user=request.GET['client_id']).order_by('-active', '-last_updated')
    else:
        vehicles_list = Vehicle.objects.filter(user__id=request.user.id).order_by('-active', '-last_updated')

    client_id = request.GET.get('client_id', None)
    if client_id and (request.user.is_staff or request.user.is_superuser):
        client = User.objects.filter(id=client_id)[0]
    else:
        client = request.user

    print len(vehicles_list)
    context = RequestContext(request, {
        'user': request.user,
        'vehicles': vehicles_list,
        'client': client,
    })

    template = loader.get_template('vehicles.html')
    return HttpResponse(template.render(context))


@login_required()
def friends(request):
    template = loader.get_template('friends.html')
    if request.user.is_staff or request.user.is_superuser:
        clients = User.objects.filter(id__gt=1)
    else:
        clients = User.objects.filter(id=request.user.id)
    context = RequestContext(request, {
        'user': request.user,
        'clients': clients,
    }, [])
    return HttpResponse(template.render(context))


def get_posts(user):
    """"""
    if user.is_superuser:
        post_objects = Post.objects.filter(deleted=False).order_by('-last_updated')[:10]
    else:
        post_objects = Post.objects.filter(user=user, deleted=False).order_by('-last_updated')

    for post_object in post_objects:
        print post_object.message

    return post_objects


@login_required()
def get_vehicle_select(request):
    print 'get_vehicle_select'
    if request.user.is_superuser:
        vehicle_objects = Vehicle.objects.all().order_by('-last_updated')
    else:
        vehicle_objects = Vehicle.objects.filter(user=request.user).order_by('-last_updated')

    html_select = ['<select class="form-control" id="vehicle_message">']
    for vehicle_object in vehicle_objects:
        html_select.append('<option value="%s">%s %s</option>' % (
            vehicle_object.id, vehicle_object.make.name, vehicle_object.model.name))

    html_select.append('</select>')

    print html_select

    return HttpResponse('\n'.join(html_select))


@csrf_protect
@login_required()
def get_vehicle_parts_table(request):
    if request.method == 'POST':
        print('POST')
        try:
            vehicle_id = request.POST['vehicle_id']
            print(vehicle_id)
        except KeyError:
            print('KeyError')
            return HttpResponseBadRequest()
    else:
        print('GET')
        try:
            vehicle_id = request.GET['vehicle_id']
            print(vehicle_id)
        except KeyError:
            print(request.GET)
            print('KeyError')
            return HttpResponseBadRequest()

    html = vehicle_parts_table(vehicle_id)

    return HttpResponse(html)


def vehicle_parts_table(vehicle_id):
    vehicle_object = Vehicle.objects.get(id=vehicle_id)
    extra_details = vehicle_object.extra_details.filter(deleted=False)

    html_table = ['<table class="table table-striped">',
                  '<tr>',
                  '<th>Key</th><th>Value</th><th>Remove</th>',
                  '</tr>']

    for ed in extra_details:
        html_table.append('<tr>')
        html_table.append('<td>')
        html_table.append(ed.key)
        html_table.append('</td>')
        html_table.append('<td>')
        html_table.append(ed.value)
        html_table.append('</td>')
        html_table.append('<td><button onclick="delDetail(%s)" class="btn btn-danger" id="del_detail" type="button">' % ed.id)
        html_table.append('<span class="fas fa-trash-alt"></span>')
        html_table.append('</button></td>')
        html_table.append('</tr>')

    html_table.append('</table>')

    return '\n'.join(html_table)


@csrf_protect
@login_required()
def get_user_edit_modal_div(request):
    try:
        client_id = int(request.GET['client_id'])
    except KeyError:
        return HttpResponseBadRequest()

    if not client_id:
        print 'no client id'
        return HttpResponseBadRequest()
    else:
        print 'client id: %s' % client_id

    user_object = User.objects.get(id=client_id)

    context = RequestContext(request, {
        'client': user_object
    })

    template = loader.get_template('user_edit_modal_div.html')

    return HttpResponse(template.render(context))


@csrf_protect
@login_required()
def get_user_info_table_div(request):
    try:
        client_id = request.GET['client_id']
    except KeyError:
        return HttpResponseBadRequest()

    if not client_id:
        return HttpResponseBadRequest()
    else:
        client_id = int(client_id)

    user_object = User.objects.get(id=client_id)

    context = RequestContext(request, {
        'client': user_object
    })

    template = loader.get_template('user_info_table_div.html')

    return HttpResponse(template.render(context))

