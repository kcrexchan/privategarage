function saveDetails() {
    var v_id = $("#vehicle_id").val();
    var mileage = $("#mileage").val();
    var year = $("#year").val();
    var oilType = $("#oil_type").val();
    var active = $("#active").is(":checked");
    $.ajax({
        url: '/edit_vehicle_details/',
        type: 'POST',
        data: {
            vehicle_id: v_id,
            mileage: mileage,
            year: year,
            oil_type: oilType,
            active: active
        }
    })
        .done(function (data) {
            console.log(data);
            $.toast({
                heading: 'Success',
                text: 'Vehicle details saved here',
                icon: 'success',
                hideAfter: 3000,
                showHideTransition: 'slide',
                afterHidden: function () {
                    window.location.replace('/vehicle?vehicle_id=' + v_id);
                }
            })
        })
        .fail(function (data) {
            console.log('failed');
            console.log(data);
             $.toast({
                heading: 'Failed',
                text: 'Unable to save details',
                icon: 'error',
                hideAfter: 3000,
                showHideTransition: 'slide',
            })
        })
        .always(function () {
            console.log('always');
        });
}

function delDetail(d_id) {
    if (confirm('Are you sure?') == false) {
        return false;
    }
    var v_id = $("#vehicle_id").val();
    $.ajax({
        url: '/del_vehicle_detail/',
        type: 'POST',
        data: {
            vehicle_id: v_id,
            detail_id: d_id
        }
    })
        .done(function (data) {
            $("#vehicle_parts").html(data);
            $.toast({
                heading: 'Success',
                text: 'Successfully removed detail',
                icon: 'success',
                hideAfter: 3000,
                showHideTransition: 'slide',
            })
        })
        .fail(function () {
            console.log("error");
        })
        .always(function (data) {
            console.log("always");
            console.log(data);
        });
}

function addDetail() {
    console.log('add_detail clicked');
    var detail_key = $("#detail_key").val();
    if (detail_key == null || detail_key == "") {
        $.toast({
            heading: 'Warning',
            text: 'Key is empty',
            icon: 'warning',
            hideAfter: 5000,
            showHideTransition: 'slide',
        })
        return false;
    }
    var detail_value = $("#detail_value").val();
    if (detail_value == null || detail_value == "") {
        $.toast({
            heading: 'Warning',
            text: 'Value is empty',
            icon: 'warning',
            hideAfter: 5000,
            showHideTransition: 'slide',
        })

        return false;
    }
    var v_id = $("#vehicle_id").val();
    $.ajax({
        url: "/add_vehicle_extra_detail/",
        type: "POST",
        data: {
            key: detail_key,
            value: detail_value,
            vehicle_id: v_id
        }
    })
        .done(function (data) {
            console.log(data);
            $("#detail_key").val('');
            $("#detail_value").val('');
            $("#vehicle_parts").html(data);
            $.toast({
                heading: 'Success',
                text: 'Extra details added',
                icon: 'success',
                hideAfter: 3000,
                showHideTransition: 'slide',
                afterHidden: function () {
                    window.location.replace('/vehicle?vehicle_id=' + v_id)
                }
            })
        })
        .fail(function () {
            console.log("error");
        })
        .always(function (data) {
            console.log("done");
        })
}

